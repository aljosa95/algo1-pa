package ad1.ss17.pa;

import java.util.*;

public class Graph {
    // ArrayList: get O(1), add O(1) TreeSet: add O(logn), contains O(logn)

    private ArrayList<TreeSet<Integer>> graph;
    private int nodesNum;
    private int edgesNum;

    private TreeSet<Integer> copyTree ;    // for saving elements from dfs
    private boolean[] discovered;           // for marking discovered elements in dfs
    private List<Integer> rankIn;          // for getting rank of element in topological sorting
    private boolean dagIsTrue = false;          // for saving the result from DAG
    private boolean dagIsFalse = false;     // for saving when graph is not DAG
    private boolean isStronglyConnected = false; // from second time constant
    private boolean isNotStrongConn = false;       // from second time constant
    private int index;                      // index in tarjan dfs
    private boolean[] visited;              // marking visited nodes in tarjan
    private Deque<Integer> list;               // adding nodes as they are visited
    private int[] low;              // lowest index that can be reached from this node
    private boolean componentChecked = false;
    private int[] nodeHolder;           // show us in which component is each node
    private int component = 0;             // component counter

    public Graph(int n) {
        this.nodesNum = n;
        graph = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            graph.add(new TreeSet<>());
        }
    }

    public int numberOfNodes() {
        return this.nodesNum;
    }

    public int numberOfEdges() {
        return this.edgesNum;
    }

    public void addEdge(int v, int w) {
        if(!this.graph.get(v).contains(w) && v>=0 && v<this.nodesNum && w>=0 && w<this.nodesNum && v!=w){
            graph.get(v).add(w);
            this.edgesNum++;
        }
        dagIsFalse = false;
        dagIsTrue = false;
        isStronglyConnected = false;
        isNotStrongConn = false;
        componentChecked = false;
    }

    public void addAllEdges(int v) {
        for (int i = 0; i < numberOfNodes() ; i++) {
            this.addEdge(v,i);
        }
    }

    public void deleteEdge(int v, int w) {
        if(this.graph.get(v).contains(w)){
            graph.get(v).remove(w);
            this.edgesNum--;
        }
        dagIsFalse = false;
        dagIsTrue = false;
        isStronglyConnected = false;
        isNotStrongConn = false;
        componentChecked = false;
    }

    public void deleteAllEdges(int v) {
        while (!graph.get(v).isEmpty()) {
            deleteEdge(v, graph.get(v).first());
        }
    }

    public void dfs(int start){                  // DFS for reachableNodes
        discovered[start] = true;
        Iterator<Integer> iterator =  graph.get(start).iterator(); // iterator contains all neighbors

        while(iterator.hasNext()){
            int v = iterator.next();
            if(!discovered[v]) {
                copyTree.add(v);
                dfs(v);
            }
        }
    }

    public Iterable<Integer> getReachableNodes(int v) {
        List<Integer> nodes = new LinkedList<Integer>();
        discovered  = new boolean[numberOfNodes()];
        copyTree = new TreeSet<Integer>();
        dfs(v);
        nodes.addAll(copyTree);
        return nodes;
    }

    public boolean isDAG() {
        if (this.dagIsTrue) return true;    // from second call constant and if no changes on graph
        else if (this.dagIsFalse ) return false;  // as first comment above

        int[] count = new int[nodesNum];
        rankIn = new ArrayList<>();

        for (int i = 0; i < nodesNum; i++) {
            count[i] = 0;
        }
        for (int i = 0; i < nodesNum; i++) {
            for (int v: graph.get(i)) {
                count[v] = count[v] + 1;            // give us deg- of node v
            }
        }
        Deque<Integer> list = new LinkedList<>();
        for (int i = 0; i < nodesNum ; i++) {
            if (count[i]==0){
                list.addFirst(i);
            }
        }
        while (!list.isEmpty()){
            rankIn.add(list.peek());                        // gives us list of topological sorting
            int k = list.poll();
            for (int v: graph.get(k)) {
                count[v] = count[v] - 1;
                if (count[v]==0){
                    list.addFirst(v);
                }
            }
        }
        for (int i = 0; i < numberOfNodes() ; i++) {       // check if one node has remain with count not equal to 0
            if (count[i]!=0) {
                this.dagIsFalse = true;
                return false;
            }
        }
        this.dagIsTrue = true;
        return true;
    }

    public int rankInOrder(int i) {
        if (!isDAG()) {
            return -1;
        }
        return rankIn.indexOf(i);
    }

    public boolean isStronglyConnected(){
        if (this.isStronglyConnected ) return true;
        if (nodesNum==1) return true;
        else if (this.isNotStrongConn || edgesNum==0 ) return false;
        else if (nodesNum>0){                   // it has to have more than one node to do tarjan
            if (!componentChecked) checkComponent(0,0);             // do tarjan if it's not been checked
            for (int k = 0; k < nodesNum; k++) {
                if (nodeHolder[k] != 0){            // soon as we one node find that's in other component give false
                    isNotStrongConn = true;
                    return false;
                }
            }
        }
        this.isStronglyConnected = true;
        return true;
    }

    public boolean checkComponent(int i, int j){
        if (!componentChecked) {                // if components have not been checked
            visited = new boolean[nodesNum];
            list = new LinkedList<>();
            low = new int[nodesNum];
            index = 0;
            nodeHolder = new int[nodesNum];             // in which component is each node
            component = 0;
            for (int k = 0; k < nodesNum; k++) {
                if (!visited[k]) {
                    tarjan(k);
                }
            }
            componentChecked = true;
        }
        return nodeHolder[i]==nodeHolder[j];
    }

    public void tarjan(int v){   // O(n + m)
        low[v] = index;
        index++;
        visited[v] = true;
        list.addFirst(v);
        int min = low[v];
        for (int w: graph.get(v)) {
            if (!visited[w]){
                tarjan(w);
            }
            if (low[w] < min){   // is w (adjazent) node with lower index than v (current node)
                min = low[w];      // when that case is set min from v on that value and so on
            }
        }
        if (min < low[v]){
            low[v] = min;    // lowest index has been changed
            return;
        }
        int n;
        do {
            n = list.poll();
            nodeHolder[n] = this.component;   // all these are in one component
            low[n] = nodesNum;              // we give it max, cause we don't want other nodes to use this low value
        }while (n!=v);          // until we don't reach root of new component
        this.component++;
    }



    public static void main(String[] args) {

    }

}


